<?php
namespace Larakit\QuickForm;

class ElementSelectTwbs extends \HTML_QuickForm2_Element_Select {

    use TraitNode;

    /**
     * @param null $name
     *
     * @return ElementTbSelect
     */
    static function laraform($name, $attributes = null, array $data = []) {
        $el = new ElementSelectTwbs($name, $attributes, $data);
        $el->addClass('form-control');

        return $el;
    }

    public function getType() {
        return 'select_twbs';
    }

    function loadOptionsCombine($options) {
        $options_values = array_values($options);
        $options        = array_combine($options_values, $options_values);

        return $this->loadOptions($options);
    }

    public function loadOptionsEmpty(array $options, $message = 'выберите', $value = 0) {
        return $this->loadOptions([$value => '- ' . $message . ' -'] + $options);
    }

    function setMultiple($val = true) {
        if($val) {
            $this->setAttribute('multiple', 'multiple');
        } else {
            $this->removeAttribute('multiple');
        }

        return $this;
    }

}