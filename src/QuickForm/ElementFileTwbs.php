<?php
namespace Larakit\QuickForm;
class ElementFileTwbs extends \HTML_QuickForm2_Element_InputFile {
    use TraitNode;

    public function getType() {
        return 'file_twbs';
    }

    /**
     * @param null $name
     *
     * @return ElementFileTwbs
     */
    static function laraform($name) {
        $el = new ElementFileTwbs($name);
        $el->addClass('form-control');
        return $el;
    }

}
