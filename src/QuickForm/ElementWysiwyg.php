<?php
namespace Larakit\QuickForm;

class ElementWysiwyg extends ElementTextareaTwbs {

    public function getType() {
        return 'wysiwyg';
    }

    /**
     * @param   $name
     *
     * @return ElementWysiwyg
     */
    static function laraform($name) {
        $el = new ElementWysiwyg($name);
        $el->addClass('js-element-wysiwyg');


        return $el;
    }
}